## Desafio de Data Engineering
O objetivo do projeto é fazer o ELT dos arquivos excel e da API do Spotify, com os dados disponíveis é feita a limpeza, transformação ( aplicando a regra de negócio ) e enviar para as tabelas analíticas no BigQuery. Todo processo precisa ser orquestrado então neste projeto foi usando o Airflow (Local e Cloud GCP).


## Resumo Geral
Para realização do desafio foi utilizado Python 3.10.9. Para executar o código localmente é necessário fazer o download do projeto dando um `Git clone` e instalar as dependências necessárias para a execução do código. Todos os comandos abaixo são executados no **CMD**, na **pasta raiz do projeto**.
Recomendo ao usuário usar uma das opções abaixo como ferramenta de criação do ambiente:

- Anaconda/Miniconda:

        Conda create –n $(basename $(pwd)) python=3.10.9
        conda activate $(basename $(pwd))

- Pyenv

        python3.10 -m venv app_env
        source app_env/bin/activate


Para instalar as dependencias necessárias para rodar o código use o seguinte comando;

    pip install -r requirements-dev.txt
    ou
    pip3 install -r requirements-dev.txt

Leva alguns minutos para completar toda a instalação e o ambiente está pronto para uso.
Às vezes quando e criado um ambiente virtual o Python pode não reconhecer o diretório e apresentar alguns erros estranhos, então é recomendado usar o comando abaixo na pasta raiz do projeto;

    export PYTHONPATH=$(pwd)


Com o ambiente criado e dependências instaladas, o usuário pode mandar o seguinte comando na pasta raiz 2rp_challenge/src.

    python main.py
    ou
    Python3 main.py


![alt text](./boticario_challange/DOC/boticario_challange.png)

## Estrutura de código
```
.
├── boticario_challange
│   ├── configs
│   │   ├── datasets
│   │   │   └── *.xlsx
│   │   ├── tests
│   │   │   └── tests_*.py
│   │   ├── validates
│   │   │   └── *.py
│   │   ├── .coveragerc
│   │   ├── Makefile
│   │   └── validator.py
│   ├── DOC
│   │   └── boticario_challange.png
│   ├── flow
│   │   └── dags
│   │       ├── data
│   │       │   └── *.xlsx
│   │       ├── script
│   │       │   └── *.py
│   │       └── pipeline-trigger-dev.py
│   │
│   ├── infra
│   │   └── terraform
│   │       ├── schemas
│   │       │   └── *.json
│   │       ├── bigquery.tf
│   │       ├── bucket.tf
│   │       ├── composer.tf
│   │       ├── main.tf
│   │       └── variable.tf
│   └── src
│       ├── tests
│       │   └── test_*.py
│       ├── __init__.py
│       ├── .coveragerc
│       ├── bq_schema.py
│       ├── gcp_bigquery.py
│       ├── main.py
│       ├── make_jobs.py
│       ├── Makefile
│       ├── requirements-dev.txt
│       ├── requirements.txt
│       ├── secund_case.py
│       ├── setup.py
│       ├── spotify.py
│       └── version.py
├── .gitignore
├── .gitlab-ci.yml
├── .python-version
├── Makefile
├── README.md
└── env.sh
```


## Como esses pipelines são executados
O [airflow Scheduler](https://airflow.apache.org/docs/apache-airflow/2.5.1/concepts/scheduler.html). Atualmente, essas agregações são executadas uma vez por dia:

- Every day at 6:30 A.M. (America/Sao_Paulo).


## Testes Automatizados
##:construction: In progress :construction:
O projeto tem duas pastas de testes localizadas em;

- boticario_challenge/boticario_challenge
    - tests
	- validates

- boticario_challenge/src
	- tests

- O usuário executa esses testes entrando em um dos dois diretórios citado acima e executando um dos comandos abaixo;
    - make test
    - make coverage

Os testes estão automatizados no CI/CD do Gitlab


## Gitlab CI/CD
O projeto é deployado pelo CI/CD do Gitlab no repositório `https://gitlab.com/MattheuSxS/boticario_challange`. Há quatro etapas a serem feitas para um deploy ser bem sucedido! São elas;

- Stages:
    - validate-config-files ~> Faz a validação dos arquivos excel
    - terraform-test        ~> Faz a validação dos arquivos terraform
    - terraform-deploy      ~> Faz o deploy da aplicação (Manual)
    - terraform-destroy     ~> Destroy a aplicação (Manual)


## Referências

- GitLab:
    - [Documento GitLab](https://docs.gitlab.com)
    - [Comando GitLab Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
    - [GitLab CI/CD](https://docs.gitlab.com/ee/ci/ )

- Terraform:
    - [Documento Terraform](https://developer.hashicorp.com/terraform/intro)
    - [Terraform Google](https://registry.terraform.io/providers/hashicorp/google/latest)

- Python:
    - [Documento Python](https://docs.python.org/3.10/)
    - [Documento Polars](https://pola-rs.github.io/polars/py-polars/html/index.html)
    - [Book Polars](https://pola-rs.github.io/polars-book/user-guide/introduction.html)
    - [Documento Pytest](https://docs.pytest.org/en/7.2.x/contents.html)
    - [Documento Coverage](https://coverage.readthedocs.io/en/6.5.0/)

- Spotify API
    - [Documento API](https://developer.spotify.com/documentation/)
    - [Buscar por itens](https://developer.spotify.com/console/get-search-item/)

- Airflow:
    - [Documento Airflow](https://airflow.apache.org/docs/)
    - [Scheduler Airflow](https://airflow.apache.org/docs/apache-airflow/1.10.13/scheduler.html)

- Google Cloud (Google Cloud Platform):
    - [Documento Composer](https://cloud.google.com/composer/docs/run-apache-airflow-dag)
    - [Documento Cloud Storage](https://cloud.google.com/storage/docs?hl=pt-br)
    - [Documento BigQuery](https://cloud.google.com/bigquery/docs?hl=pt-br)

- Docker:
    - [Documento Docker](https://docs.docker.com)
    - [Docker Airflow](https://hub.docker.com/r/apache/airflow)

- Makefile
    - [Documento Makefile](https://www.gnu.org/software/make/manual/make.html)
    - [Documento Tutorial](https://makefiletutorial.com)


| Versão Do Documento |        Editor      |    Data    |
|        :---:        |        :---:       |    :---:   |
|        1.0.0        | Matheus Dos Santos | 25/03/2023 |