.PHONY: build-env deploy-airflow-dev


build-env:
	$(shell ./env_variables.sh)


deploy-airflow-dev:
	docker run -d -p 8080:8080 -v "$(PWD)/boticario_challange/flow/dags:/opt/airflow/dags/" \
	--env "SPORTFY_CLIENT_ID=c1f546b3c0e14f88b2c159842d75115e" \
	--env "SPORTFY_CLIENT_SECRET=64cb20ea86064065918c8321a9485350" \
	--env "GCP_PROJECT_ID=gcp-data-pipeline-stack" \
	--env "GCP_PROJECT_REGION=us-east1" \
	--env "GCP_BIGQUERY_DATASET_RAW_ID=raw_sales_data" \
	--env "GCP_BIGQUERY_TABLE_RAW_ID=sales-data" \
	--env "GCP_BIGQUERY_DATASET_ANALYTICAL_ID=dataset_secund_case" \
	--env "AIRFLOW_PLACE=2" \
	--entrypoint=/bin/bash \
	--name airflow apache/airflow:2.5.1-python3.10 \
	-c '(airflow db init && \
		airflow users create --username admin --password 87654321a --firstname Matheus --lastname Santos --role Admin --email mattheusxs@gmail.com); \
	airflow webserver & \
	airflow scheduler'