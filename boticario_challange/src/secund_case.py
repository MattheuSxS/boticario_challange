import polars as pl
from loguru import logger
import re
from spotify import Spotify


class SecundCase:
    def __init__(self, project_id:str, prd_dataset:str, data_raw:dict = dict()) -> pl.DataFrame:
        self.project_id             = project_id
        self.prd_dataset            = prd_dataset
        self.data_raw               = data_raw
        self.spotify                = Spotify()
        self.df_dict_select_seasch  = self.spotify.get_data_spotify(request_type=1)
        self.df_dict_full_seasch    = self.spotify.get_data_spotify(request_type=2)



    def first_table(self) -> str:
        '''
            Tabela 1: Consolidado de vendas por ano e mês;
        '''
        self.first_table = \
            f'''
                INSERT INTO `{self.project_id}.{self.prd_dataset}.first_table` (QTD_VENDA, ANO_MES)
                SELECT
                    SUM(QTD_VENDA) AS QTD_VENDA,
                    DATE_TRUNC(DATE(STRING(DATA_VENDA)), MONTH) AS ANO_MES
                FROM
                    `gcp-data-pipeline-stack.raw_sales_data.sales-data`
                GROUP BY 2
                ORDER BY 2
            '''

        return self.first_table


    def secund_table(self) -> str:
        '''
            Tabela 2: Consolidado de vendas por marca e linha;
        '''
        self.secund_table = \
            f'''
                INSERT INTO `{self.project_id}.{self.prd_dataset}.secund_table` (MARCA, LINHA, QTD_VENDA)
                SELECT
                    MARCA,
                    LINHA,
                    SUM(QTD_VENDA) AS QTD_VENDA
                FROM
                    `gcp-data-pipeline-stack.raw_sales_data.sales-data`
                GROUP BY
                    MARCA, LINHA
                ORDER BY
                    MARCA
            '''

        return self.secund_table


    def third_table(self) -> str:
        '''
            Tabela 3: Consolidado de vendas por marca, ano e mês;
        '''
        self.third_table = \
            f'''
                INSERT INTO `{self.project_id}.{self.prd_dataset}.third_table` (ANO_MES, MARCA, QTD_VENDA)
                SELECT
                    DATE_TRUNC(DATE(STRING(DATA_VENDA)), MONTH) AS ANO_MES,
                    MARCA,
                    SUM(QTD_VENDA) AS QTD_VENDA
                FROM
                    `gcp-data-pipeline-stack.raw_sales_data.sales-data`
                GROUP BY
                    ANO_MES, MARCA
                ORDER BY
                    ANO_MES

            '''

        return self.third_table


    def fourth_table(self) -> str:
        '''
            Tabela 4: Consolidado de vendas por linha, ano e mês;
        '''
        self.fourth_table = \
        f'''
        INSERT INTO `{self.project_id}.{self.prd_dataset}.fourth_table` (ANO_MES, LINHA, QTD_VENDA)
        SELECT
            DATE_TRUNC(DATE(STRING(DATA_VENDA)), MONTH) AS ANO_MES,
            LINHA,
            SUM(QTD_VENDA) AS QTD_VENDA
        FROM
            `gcp-data-pipeline-stack.raw_sales_data.sales-data`
        GROUP BY
            ANO_MES, LINHA
        ORDER BY
            ANO_MES
        '''

        return self.fourth_table

    '''
        Modulo do Api Spotify
    '''
    def fifth_table(self) -> pl.DataFrame:
        self.fifth_table = self.df_dict_full_seasch.copy()
        self.fifth_table = pl.from_dicts(self.fifth_table)
        self.fifth_table = self.fifth_table.select(
            pl.col(['name', 'description', 'id', 'total_episodes'])
        )

        return self.fifth_table


    def sixth_table(self) -> pl.DataFrame:
        self.sixth_table = self.df_dict_select_seasch.copy()
        self.sixth_table = pl.from_dicts(self.sixth_table)

        self.sixth_table =self.sixth_table.with_columns(
            pl.col('release_date').str.strptime(pl.Date, fmt='%Y-%m-%d')
            .cast(pl.Date))

        return self.sixth_table


    def seventh_table(self) -> pl.DataFrame:
        self.seventh_table = self.df_dict_select_seasch.copy()
        self.seventh_table = pl.from_dicts(self.seventh_table)

        self.seventh_table =self.seventh_table.with_columns(
            pl.col('release_date').str.strptime(pl.Date, fmt='%Y-%m-%d')
            .cast(pl.Date))

        self.seventh_table = self.seventh_table.filter(
            (pl.col('name').str.contains(r'\bBoticário\b')) &
            (pl.col('name').str.contains(r'\bGrupo Boticário\b')) &
            (pl.col('description').str.contains(r'\bGrupo Boticário\b')) &
            (pl.col('description').str.contains(r'\bGrupo Boticário\b')))

        return self.seventh_table
