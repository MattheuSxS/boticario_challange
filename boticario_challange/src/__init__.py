import os
from loguru import logger

def cloud_auth():
    try:
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r'gcp-data-pipeline-stack-36a9636b1b74.json'
        logger.info('google cloud platform credentials configured successfully!')
    except Exception as e:
        raise f'problem reading .json file: {e}'


cloud_auth()