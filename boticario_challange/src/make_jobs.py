from loguru import logger
import os
import polars as pl
import re


def __raw_df_creation() -> pl.DataFrame:

    logger.info("Raw Dataframe creation in progress")

    file_path = '../configs/datasets'
    files = os.listdir(file_path)

    df_default = pl.DataFrame()

    for filename in files:
        full_path = '{}/{}'.format(file_path, filename)
        if re.search('.xlsx', filename):
            data_append = pl.read_excel(source=full_path)
            df_default = pl.concat([df_default, data_append], how="vertical")


    logger.info(f"Raw Dataframe successfully created!")

    return df_default


def __raw_df_transformation() -> pl.DataFrame:

    logger.info(f"Transformation on raw data started")
    raw_data = __raw_df_creation()
    date_format = '%m-%d-%y'
    raw_data = raw_data.with_columns(
        pl.col('DATA_VENDA')
        .str.strptime(pl.Date, fmt=date_format)
        .cast(pl.Date))

    raw_data = raw_data.with_columns(
        pl.col('ID_MARCA').cast(pl.Utf8))

    raw_data = raw_data.with_columns(
        pl.col('ID_LINHA').cast(pl.Utf8))

    # logger.info("finished raw data transformation")

    return raw_data


def get_data_raw() -> pl.DataFrame:
    return __raw_df_transformation()

