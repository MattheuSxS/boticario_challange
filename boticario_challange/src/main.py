from loguru import logger
from make_jobs import get_data_raw
import os
from gcp_bigquery import BigQuery
from google.cloud import bigquery
from secund_case import SecundCase


__app_name__    = 'boticario_challange'

project_id      = os.environ.get('GCP_PROJECT_ID')
raw_dataset_id  = os.environ.get('GCP_BIGQUERY_DATASET_RAW_ID')
raw_table_id    = os.environ.get('GCP_BIGQUERY_TABLE_RAW_ID')
prd_dataset     = os.environ.get('GCP_BIGQUERY_DATASET_ANALYTICAL_ID')


def main() -> None:

    logger.info('initialized application')

    '''
        filesystem local
    '''
    bronze_data = get_data_raw()

    # '''
    #     BigQuery --> raw data
    # '''
    bigquery_client = bigquery.Client()
    bq = BigQuery(bigquery_client=bigquery_client, project_id=project_id)

    bq.send_bigquery(raw_data=bronze_data,
                   dataset_id=raw_dataset_id, table_id=raw_table_id)

    # query =\
    # f'''
    #     SELECT
    #         ID_MARCA,
    #         MARCA,
    #         ID_LINHA,
    #         LINHA,
    #         DATA_VENDA,
    #         QTD_VENDA
    #     FROM
    #         `{project_id}.{raw_dataset_id}.{raw_table_id}`
    # '''

    # df_bronze_bq = bq.get_data_raw_bq(query=query)
    # print(df_bronze_bq)

    secund_case = SecundCase(project_id=project_id, prd_dataset=prd_dataset)

    logger.info(f'Tabela 1: Consolidado de vendas por ano e mês')
    bq.insert_query_bigquery(secund_case.first_table())

    logger.info(f'Tabela 2: Consolidado de vendas por marca e linha')
    bq.insert_query_bigquery(secund_case.secund_table())

    logger.info(f'Tabela 3: Consolidado de vendas por marca, ano e mês')
    bq.insert_query_bigquery(secund_case.third_table())

    logger.info(f'Tabela 4: Consolidado de vendas por linha, ano e mês')
    bq.insert_query_bigquery(secund_case.fourth_table())

    logger.info(f'Tabela 5: Somente os campos ~> name, description, id, total_episodes')
    bq.send_bigquery(raw_data=secund_case.fifth_table(),
                     dataset_id=prd_dataset, table_id='fifth_table')

    logger.info(f'Tabela 6: Resultado de todos os episódios.')
    bq.send_bigquery(raw_data=secund_case.sixth_table(),
                     dataset_id=prd_dataset, table_id='sixth_table')

    logger.info(f'Tabela 7: Só episódios com participação do Grupo Boticário.')
    bq.send_bigquery(raw_data=secund_case.seventh_table(),
                     dataset_id=prd_dataset, table_id='seventh_table')

    logger.info('application finished and all data layers loaded successfully!')


if __name__ == "__main__":
    main()