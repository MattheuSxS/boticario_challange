variable "project" {
    type    = map(string)
    default = {
        dev = "gcp-data-pipeline-stack"
        prd = "gcp-data-pipeline-stack-prd"
    }
}

variable "region" {
    type    = string
    default = "us-east1"
}

variable "zone" {
    type    = string
    default = "us-east1-b"
}

#   ****************************************************    #
#                           BigQuery                        #
#   ****************************************************    #
variable "bigquery_dataset_raw" {
    type    = string
    default = "raw_sales_data"
}

variable "bigquery_table_raw" {
    type    = string
    default = "sales-data"
}

variable "bigquery_dataset_curated" {
    type    = string
    default = "dataset_secund_case"
}

#   ****************************************************    #
#                         cloud Composer                    #
#   ****************************************************    #

variable "cloud_composer_name" {
    type    = string
    default = "airflow-boticario"
}

#   ****************************************************    #
#                         Cloud  Bucker                     #
#   ****************************************************    #
variable "cloud_storage_name" {
    type    = string
    default = "bkt-airflow-boticario"
}

variable "stotage_class_standard" {
    type    = string
    default = "STANDARD"
}
