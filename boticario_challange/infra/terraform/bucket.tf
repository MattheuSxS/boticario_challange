
resource "google_storage_bucket_object" "my_dags" {
    depends_on  = [google_composer_environment.airflow-boticario]

    for_each    = fileset("../../flow/dags/", "**.py")
    name        = "dags/${each.key}"
    bucket      = replace(replace(google_composer_environment.airflow-boticario.config[0].dag_gcs_prefix, "gs://", ""), "/dags", "")
    source      = "../../flow/dags/${each.value}"
}


resource "google_storage_bucket_object" "modules_files" {
    depends_on  = [google_composer_environment.airflow-boticario]

    for_each    = fileset("../../flow/dags/scripts/", "**.py")
    name        = "dags/scripts/${each.value}"
    bucket      = replace(replace(google_composer_environment.airflow-boticario.config[0].dag_gcs_prefix, "gs://", ""), "/dags", "")
    source      = "../../flow/dags/scripts/${each.value}"
}


resource "google_storage_bucket_object" "json_key" {
    depends_on  = [google_composer_environment.airflow-boticario]

    for_each    = fileset("../../flow/dags/scripts/", "**.json")
    name        = "dags/scripts/${each.value}"
    bucket      = replace(replace(google_composer_environment.airflow-boticario.config[0].dag_gcs_prefix, "gs://", ""), "/dags", "")
    source      = "../../flow/dags/scripts/${each.value}"
}


resource "google_storage_bucket_object" "data_files" {
    depends_on  = [google_composer_environment.airflow-boticario]

    for_each        = fileset("../../configs/datasets/", "**.xlsx")
    name            = "data/${each.value}"
    bucket          = replace(replace(google_composer_environment.airflow-boticario.config[0].dag_gcs_prefix, "gs://", ""), "/dags", "")
    source          = "../../configs/datasets/${each.value}"
    content_type    = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
}
