resource "google_composer_environment" "airflow-boticario" {
    project = var.project[terraform.workspace]
    name    = var.cloud_composer_name
    region  = var.region

    config {

        node_count = 3

        node_config {
            disk_size_gb    = 100
            zone            = var.zone
            machine_type    = "n1-standard-2"
        }

        software_config {
            image_version   = "composer-1.20.10-airflow-2.4.3"
            python_version  = "3"
            pypi_packages   = {
                    requests    = "==2.28.2",
                    polars      = "==0.16.13",
                    xlsx2csv    = "==0.8.1",
            }
            env_variables   = {
                GCP_PROJECT_ID                      = var.project[terraform.workspace],
                GCP_PROJECT_REGION                  = "us-east1",
                GCP_BIGQUERY_DATASET_RAW_ID         = "raw_sales_data",
                GCP_BIGQUERY_TABLE_RAW_ID           = "sales-data",
                GCP_BIGQUERY_DATASET_ANALYTICAL_ID  = "dataset_secund_case",
                SPORTFY_CLIENT_ID                   = "c1f546b3c0e14f88b2c159842d75115e",
                SPORTFY_CLIENT_SECRET               = "64cb20ea86064065918c8321a9485350",
                AIRFLOW_PLACE                       = "1"
            }
        }

        web_server_config {
            machine_type = "composer-n1-webserver-2"
        }
    }

    lifecycle {
        prevent_destroy = false
    }

}
