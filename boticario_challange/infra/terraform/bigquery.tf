resource "google_bigquery_dataset" "bigquery_dataset_raw" {
    dataset_id                 = var.bigquery_dataset_raw
    friendly_name              = var.bigquery_dataset_raw
    description                = "Analytical dataset - test tecnico boticário"
    project                    = var.project[terraform.workspace]
    location                   = var.region
    delete_contents_on_destroy = false

    labels = {
        env = "default"
        }
}

resource "google_bigquery_table" "bigquery_table_raw" {
    project             = var.project[terraform.workspace]
    dataset_id          = var.bigquery_dataset_raw
    table_id            = var.bigquery_table_raw
    deletion_protection = false
    depends_on = [google_bigquery_dataset.bigquery_dataset_raw]

    # time_partitioning {
    #     type    = "DAY"
    #     field   = "DATA_VENDA"
    # }

    labels = {
        "created_by": "terraform",
        "layer": "raw"
    }

    clustering = [
        "MARCA",
        "LINHA"
    ]

    schema = file("${path.module}/schemas/raw_sales_data.json")

}

resource "google_bigquery_dataset" "bigquery_dataset_curated" {
    dataset_id                 = var.bigquery_dataset_curated
    friendly_name              = var.bigquery_dataset_curated
    description                = "Analytical dataset - test tecnico boticário"
    project                    = var.project[terraform.workspace]
    location                   = var.region
    delete_contents_on_destroy = false

    labels = {
        env = "default"
        }
}


resource "google_bigquery_table" "first_table" {
    depends_on = [google_bigquery_dataset.bigquery_dataset_curated]

    project             = var.project[terraform.workspace]
    dataset_id          = var.bigquery_dataset_curated
    table_id            = "first_table"
    deletion_protection = false

    # time_partitioning {
    #     type    = "DAY"
    #     field   = "DATA_VENDA"
    # }

    labels = {
        "created_by": "terraform",
        "layer": "curated"
    }

    # clustering = [
    #     "MARCA",
    #     "LINHA"
    # ]

    schema = file("${path.module}/schemas/first_table.json")

}


resource "google_bigquery_table" "secund_table" {
    depends_on = [google_bigquery_dataset.bigquery_dataset_curated]

    project             = var.project[terraform.workspace]
    dataset_id          = var.bigquery_dataset_curated
    table_id            = "secund_table"
    deletion_protection = false

    # time_partitioning {
    #     type    = "DAY"
    #     field   = "DATA_VENDA"
    # }

    labels = {
        "created_by": "terraform",
        "layer": "curated"
    }

    clustering = [
        "MARCA",
        "LINHA"
    ]

    schema = file("${path.module}/schemas/secund_table.json")

}

resource "google_bigquery_table" "third_table" {
    depends_on = [google_bigquery_dataset.bigquery_dataset_curated]

    project             = var.project[terraform.workspace]
    dataset_id          = var.bigquery_dataset_curated
    table_id            = "third_table"
    deletion_protection = false

    # time_partitioning {
    #     type    = "DAY"
    #     field   = "DATA_VENDA"
    # }

    labels = {
        "created_by": "terraform",
        "layer": "curated"
    }

    clustering = [
        "ANO_MES",
        "MARCA"
    ]

    schema = file("${path.module}/schemas/third_table.json")

}

resource "google_bigquery_table" "fourth_table" {
    depends_on = [google_bigquery_dataset.bigquery_dataset_curated]

    project             = var.project[terraform.workspace]
    dataset_id          = var.bigquery_dataset_curated
    table_id            = "fourth_table"
    deletion_protection = false

    # time_partitioning {
    #     type    = "DAY"
    #     field   = "DATA_VENDA"
    # }

    labels = {
        "created_by": "terraform",
        "layer": "curated"
    }

    clustering = [
        "ANO_MES",
        "LINHA"
    ]

    schema = file("${path.module}/schemas/fourth_table.json")

}

resource "google_bigquery_table" "fifth_table" {
    depends_on = [google_bigquery_dataset.bigquery_dataset_curated]

    project             = var.project[terraform.workspace]
    dataset_id          = var.bigquery_dataset_curated
    table_id            = "fifth_table"
    deletion_protection = false

    # time_partitioning {
    #     type    = "DAY"
    #     field   = "DATA_VENDA"
    # }

    labels = {
        "created_by": "terraform",
        "layer": "curated"
    }

    schema = file("${path.module}/schemas/fifth_table.json")

}

resource "google_bigquery_table" "sixth_table" {
    depends_on = [google_bigquery_dataset.bigquery_dataset_curated]

    project             = var.project[terraform.workspace]
    dataset_id          = var.bigquery_dataset_curated
    table_id            = "sixth_table"
    deletion_protection = false

    # time_partitioning {
    #     type    = "DAY"
    #     field   = "DATA_VENDA"
    # }

    labels = {
        "created_by": "terraform",
        "layer": "curated"
    }

    schema = file("${path.module}/schemas/sixth_table.json")

}

resource "google_bigquery_table" "seventh_table" {
    depends_on = [google_bigquery_dataset.bigquery_dataset_curated]

    project             = var.project[terraform.workspace]
    dataset_id          = var.bigquery_dataset_curated
    table_id            = "seventh_table"
    deletion_protection = false

    # time_partitioning {
    #     type    = "DAY"
    #     field   = "DATA_VENDA"
    # }

    labels = {
        "created_by": "terraform",
        "layer": "curated"
    }

    schema = file("${path.module}/schemas/seventh_table.json")

}