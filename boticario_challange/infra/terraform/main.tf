terraform {
    required_providers {
        google = {
            source = "hashicorp/google"
            version = "4.57.0"
            }
        }
    backend "gcs" {
        bucket = "bkt-terraform-boticario-tfstate"
        prefix = "terraform/state"
    }
}

provider "google" {
    project = var.project[terraform.workspace]
    region  = var.region
}



