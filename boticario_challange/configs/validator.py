import os
import logging
import argparse
import polars as pl
import re


logging.basicConfig(
    format='%(asctime)s - %(filename)s:%(lineno)d - %(levelname)s - %(message)s', level='INFO')


def is_valid(data: pl.DataFrame) -> bool:
    if data.is_empty():
        raise Exception('empty dataframe')

    return True


if __name__ == '__main__':

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "-c", "--check", help="execute validates for config files.", required=True, action="store_true"
        )

        args = parser.parse_args()

        if args.check:
            configs_path = os.path.join(os.getcwd(), 'datasets/')

            files = os.listdir(configs_path)

            for filename in files:

                if re.search('.xlsx', filename):

                    logging.info(f"ready for validation file: {filename}")
                    data_raw = pl.read_excel(source=f'{configs_path}{filename}')

                    if is_valid(data_raw):
                        logging.info(f"sucessfull validation for: {filename}")

                        del data_raw

    except Exception as error:
        raise(f"Invalid csv file! Error: {error}.")
