import os
import logging


def cloud_auth(place:int = 1):


    try:
        if place == 1:
            os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r'/home/airflow/gcs/dags/scripts/gcp-data-pipeline-stack-36a9636b1b74.json'
        elif place == 2:
            os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r'/opt/airflow/dags/scripts/gcp-data-pipeline-stack-36a9636b1b74.json'

        logging.info('google cloud platform credentials configured successfully!')
    except Exception as e:
        raise f'problem reading .json file: {e}'


airflow_place = os.environ.get('AIRFLOW_PLACE', 2)
cloud_auth(airflow_place)