import requests
import os
import json
import logging


logging.basicConfig(
    format=("%(asctime)s | %(levelname)s | File_name ~> %(module)s.py "
            "| Function ~> %(funcName)s | Line ~> %(lineno)d  ~~>  %(message)s"),
    level=logging.DEBUG
    )


class Spotify:
    def __init__(self) -> None:
        self.client_id       = os.environ.get('SPORTFY_CLIENT_ID')
        self.client_secret   = os.environ.get('SPORTFY_CLIENT_SECRET')

    def __get_token_spotify(self) -> str:


        auth_url = 'https://accounts.spotify.com/api/token'

        try:
            logging.info('The process to get the token has started')
            auth_response = requests.post(auth_url, {
                'grant_type': 'client_credentials',
                'client_id': self.client_id,
                'client_secret': self.client_secret
            })

            logging.info('Token generated successfully!')
            spotify_token = json.loads(auth_response.text)["access_token"]


        except ValueError as errors:
            raise f'Unable to get token: {errors}'

        return spotify_token


    def __get_auth_header(self) -> dict:

        spotify_token = self.__get_token_spotify()
        data_dict = \
            {
                "Content-Type": "application/json",
                "Host": "api.spotify.com",
                "Authorization": f"Bearer {spotify_token}"
            }

        return data_dict


    def get_data_spotify(self, request_type:str, limit:int = 50) -> dict:
        '''
        request_type: type of request:
            1  ~> Search for Item
            2  ~> Show Episodes
        '''
        if request_type == '1':
            url = f"https://api.spotify.com/v1/shows/1oMIHOXsrLFENAeM743g93/episodes?market=BR&limit={limit}"

        if request_type == '2':
            url = f"https://api.spotify.com/v1/search?q=Data%20Hackers&type=show&market=BR&limit={limit}"

        headers = self.__get_auth_header()

        logging.info('spotify api request process started')

        response = requests.get(url=url, headers=headers)
        assert response.status_code == 200, \
            f'problem in the request! Status: {response.status_code}'

        logging.info('Request accepted and data retrieved successfully!')

        if request_type == '1':
            return response.json()['items']

        if request_type == '2':
            return response.json()['shows']['items']
