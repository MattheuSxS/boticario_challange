import polars as pl

def raw_schema() -> dict:

    raw_schema = dict(
        ID_MARCA=pl.Utf8,
        MARCA=pl.Utf8,
        ID_LINHA=pl.Utf8,
        LINHA=pl.Utf8,
        DATA_VENDA=pl.Date,
        QTD_VENDA=pl.Int64
    )

    return raw_schema
