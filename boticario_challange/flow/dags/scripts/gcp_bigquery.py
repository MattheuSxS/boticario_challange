import polars as pl
from scripts.bq_schema import raw_schema
from google.api_core import exceptions
import logging


logging.basicConfig(
    format=("%(asctime)s | %(levelname)s | File_name ~> %(module)s.py "
            "| Function ~> %(funcName)s | Line ~> %(lineno)d  ~~>  %(message)s"),
    level=logging.DEBUG
    )

class BigQuery:
    def __init__(self, bigquery_client, project_id):
        self.client     = bigquery_client
        self.project_id = project_id


    def __insert_bigquery(self, raw_data:pl.DataFrame, dataset_id:str, table_id:str) -> None:

        logging.info(f"Started ingesting data into {dataset_id}.{table_id} table")

        self.data_dict  = raw_data.to_dict()
        self.data_list  = [{k: v[i] for k, v in self.data_dict.items()} for i in range(len(raw_data))]
        self.path_table = '{}.{}.{}'.format(self.project_id, dataset_id, table_id)
        table           = self.client.get_table(self.path_table)
        insert_job      = self.client.insert_rows(table, self.data_list)

        if insert_job != []:
            raise f'Insert error failed: {insert_job}'

        logging.info(f"Success! {len(self.data_list)} rows were inserted.")


    def send_bigquery(self, raw_data: pl.DataFrame, dataset_id:str, table_id:str) -> None:
        return self.__insert_bigquery(raw_data=raw_data, dataset_id=dataset_id, table_id=table_id)


    def __get_data_bigquery(self, query:str):

        query_job   = self.client.query(query)
        result      = query_job.result()
        data_raw    = {field.name: [] for field in result.schema}

        for row in result:
            for field in result.schema:
                data_raw[field.name].append(row[field.name])

        df_raw = pl.DataFrame(data=data_raw, schema=raw_schema())

        return df_raw


    def get_data_raw_bq(self, query:str):
        return self.__get_data_bigquery(query)


    def __insert_query_bigquery(self, query:str):

        try:
            query_job = self.client.query(query)
            logging.info(f'Insert successfully done! Job ID: {query_job.job_id}')

        except exceptions.GoogleAPICallError as erro:
            raise f'Unable to execute query error: {erro}'


    def insert_query_bigquery(self, query:str):
        return self.__insert_query_bigquery(query)


