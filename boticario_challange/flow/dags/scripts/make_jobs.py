import os
import polars as pl
import re
import logging


logging.basicConfig(
    format=("%(asctime)s | %(levelname)s | File_name ~> %(module)s.py "
            "| Function ~> %(funcName)s | Line ~> %(lineno)d  ~~>  %(message)s"),
    level=logging.DEBUG
    )


def __raw_df_creation(place:str = '2') -> pl.DataFrame:

    logging.info("Raw Dataframe creation in progress")

    '''
        place: Local onde está rodando o airflow
            1   ~>  Cloud
            2   ~>  Local
    '''

    if place == '1':
        file_path = '/home/airflow/gcsfuse/data/'

    if place == '2':
        file_path = '/opt/airflow/dags/data/'


    df_default = pl.DataFrame()
    files = os.listdir(file_path)
    for filename in files:
        full_path = '{}{}'.format(file_path, filename)
        if re.search('.xlsx', filename):
            data_append = pl.read_excel(source=full_path)
            df_default = pl.concat([df_default, data_append], how="vertical")


    logging.info(f"Raw Dataframe successfully created!")

    return df_default


def __raw_df_transformation(place:str = '2') -> pl.DataFrame:

    logging.info(f"Transformation on raw data started")
    raw_data = __raw_df_creation(place)
    date_format = '%m-%d-%y'
    raw_data = raw_data.with_columns(
        pl.col('DATA_VENDA')
        .str.strptime(pl.Date, fmt=date_format)
        .cast(pl.Date))

    raw_data = raw_data.with_columns(
        pl.col('ID_MARCA').cast(pl.Utf8))

    raw_data = raw_data.with_columns(
        pl.col('ID_LINHA').cast(pl.Utf8))

    logging.info("finished raw data transformation")

    return raw_data


def get_data_raw(place:str = '2') -> pl.DataFrame:
    return __raw_df_transformation(place)
