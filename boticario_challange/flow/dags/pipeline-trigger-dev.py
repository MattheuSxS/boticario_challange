from datetime import timedelta, datetime, timezone
from airflow.decorators import dag, task
import os
import logging
from scripts.make_jobs import get_data_raw
from scripts.gcp_bigquery import BigQuery
from scripts.secund_case import SecundCase
from google.cloud import bigquery


logging.basicConfig(
    format=("%(asctime)s | %(levelname)s | File_name ~> %(module)s.py "
            "| Function ~> %(funcName)s | Line ~> %(lineno)d  ~~>  %(message)s"),
    level=logging.DEBUG
    )


timezone        = timezone(timedelta(hours=-3))
project_id      = os.environ.get('GCP_PROJECT_ID')
raw_dataset_id  = os.environ.get('GCP_BIGQUERY_DATASET_RAW_ID')
raw_table_id    = os.environ.get('GCP_BIGQUERY_TABLE_RAW_ID')
prd_dataset     = os.environ.get('GCP_BIGQUERY_DATASET_ANALYTICAL_ID')
service_account = os.environ.get('GOOGLE_APPLICATION_CREDENTIALS')
airflow_place   = os.environ.get('AIRFLOW_PLACE', 2)


def main() -> None:

    logging.info('initialized application')

    '''
        filesystem local
    '''
    bronze_data = get_data_raw(place=airflow_place)

    # # '''
    # #     BigQuery --> raw data
    # # '''
    bigquery_client = bigquery.Client()
    bq = BigQuery(bigquery_client=bigquery_client, project_id=project_id)

    logging.info(f'Sending raw data to bigquery: {project_id}:{raw_dataset_id}.{raw_table_id}')
    bq.send_bigquery(raw_data=bronze_data,
                   dataset_id=raw_dataset_id, table_id=raw_table_id)

    # '''
    #     BigQuery --> Data Product
    # '''
    secund_case = SecundCase(project_id=project_id, prd_dataset=prd_dataset)

    logging.info(f'Tabela 1: Consolidado de vendas por ano e mês')
    bq.insert_query_bigquery(secund_case.first_table())

    logging.info(f'Tabela 2: Consolidado de vendas por marca e linha')
    bq.insert_query_bigquery(secund_case.secund_table())

    logging.info(f'Tabela 3: Consolidado de vendas por marca, ano e mês')
    bq.insert_query_bigquery(secund_case.third_table())

    logging.info(f'Tabela 4: Consolidado de vendas por linha, ano e mês')
    bq.insert_query_bigquery(secund_case.fourth_table())

    logging.info(f'Tabela 5: Somente os campos ~> name, description, id, total_episodes')
    bq.send_bigquery(raw_data=secund_case.fifth_table(),
                     dataset_id=prd_dataset, table_id='fifth_table')

    logging.info(f'Tabela 6: Resultado de todos os episódios.')
    bq.send_bigquery(raw_data=secund_case.sixth_table(),
                     dataset_id=prd_dataset, table_id='sixth_table')

    logging.info(f'Tabela 7: Só episódios com participação do Grupo Boticário.')
    bq.send_bigquery(raw_data=secund_case.seventh_table(),
                     dataset_id=prd_dataset, table_id='seventh_table')

    logging.info('application finished and all data layers loaded successfully!')


default_args = {
    'dag_id': 'boticario_challange',
    'start_date': datetime(2023, 3, 20, tzinfo=timezone),
    'retries': 3,
    'retry_delay': timedelta(minutes=60),
    'description': "This DAG triggers the pipeline job",
    'project_id': project_id,
    'tags': ["boticario_challange"]
}

@dag(default_args=default_args, schedule_interval='30 6 * * *')

def exec_pipeline_dag():

    @task()
    def push_data_task():
        main()

    push_data_task()


exec_pipeline_dag()